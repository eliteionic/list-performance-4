import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'example-one', loadChildren: './example-one/example-one.module#ExampleOnePageModule' },
  { path: 'example-two', loadChildren: './example-two/example-two.module#ExampleTwoPageModule' },
  { path: 'example-three', loadChildren: './example-three/example-three.module#ExampleThreePageModule' },
  { path: 'example-four', loadChildren: './example-four/example-four.module#ExampleFourPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
