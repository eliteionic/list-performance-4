import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleFourPage } from './example-four.page';

describe('ExampleFourPage', () => {
  let component: ExampleFourPage;
  let fixture: ComponentFixture<ExampleFourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleFourPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleFourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
