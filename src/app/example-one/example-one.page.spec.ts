import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleOnePage } from './example-one.page';

describe('ExampleOnePage', () => {
  let component: ExampleOnePage;
  let fixture: ComponentFixture<ExampleOnePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleOnePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleOnePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
