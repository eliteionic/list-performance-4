import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-one',
  templateUrl: './example-one.page.html',
  styleUrls: ['./example-one.page.scss'],
})
export class ExampleOnePage implements OnInit {

  public items: any[] = new Array(500);

  constructor() { }

  ngOnInit() {

  }

}
