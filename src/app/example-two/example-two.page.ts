import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example-two',
  templateUrl: './example-two.page.html',
  styleUrls: ['./example-two.page.scss'],
})
export class ExampleTwoPage implements OnInit {

  public items: any[] = new Array(500);

  constructor() { }

  ngOnInit() {

  }

}
